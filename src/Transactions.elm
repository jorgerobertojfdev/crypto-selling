module Transactions exposing (..)

import Html exposing (..)

type alias Transaction = 
  {
    coin : String,
    quantity : Float,
    money : Float,
    operation : String
  }

type alias Model = (List Transaction)


type Msg
    = CreateTransaction String


view : Model -> Html Msg
view model =
    div []
        [ text "New Html Program" ]
