module Main exposing (..)
import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Task 
import Coins
import Transactions
import List 
import GetQuotations as Quotations
import Dict 

type alias Customer = 
  {
      name: String,
      email: String,
      wallet: Float,
      stored: Bool
  }

type alias Model = 
  {
      loading : Bool,
      customer : Customer,
      quotations : Quotations.Model,
      coinActions : Coins.Model,
      transactions : Transactions.Model
  }
    

initialState : () -> (Model, Cmd Msg)
initialState _ = 
    (
      {
        loading    = False,
        customer   = { name = "", email = "", wallet = 0.0, stored = False },
        quotations = {
          brita    = {
            value = []
          },
          bitcoin  = {
            data = {
              sell = 0.0,
              buy = 0.0,
              date = ""
            }
          }
        },
        coinActions = {
          value = 0.0,
          operation = "",
          selected = ""
        },
        transactions = []
      }, 
      Cmd.batch [
        Cmd.map QuotationsMsg Quotations.getBritaQuotation,
        Cmd.map QuotationsMsg Quotations.getBitcoinQuotation
      ]
    )

type Msg = 
    NoOp
    | UpdateName String 
    | UpdateEmail String
    | SaveCustomer
    | LoadingQuotations
    | LoadedQuotations
    | QuotationsMsg Quotations.Msg
    | CoinsMsg Coins.Msg
    | TransactionsMsg Transactions.Msg

updateCustomer : (Customer -> Customer) -> Model -> Model
updateCustomer fn model = 
    { model | customer = fn model.customer }

executeOperationCoin : Model -> String -> (Model, Cmd Msg)
executeOperationCoin model operation = 
  let
    (transactionsUpdated, cmd) = 
      update (TransactionsMsg (Transactions.CreateTransaction operation)) model
    oldValue = model.coinActions
    newCoinAction = { oldValue | value = 0 }
  in
    ({ transactionsUpdated | coinActions = newCoinAction }, Cmd.none)

getTransactions : Transactions.Model -> String -> Float
getTransactions transactions coin =
  let
    transactionsItem = transactions 
      |> List.partition (\x -> x.coin == coin) 
      |> Tuple.first
      |> List.partition (\x -> x.operation == "Buy")
      |> Tuple.mapBoth (List.map (\x -> x.money)) (List.map(\x -> negate x.money))    

    transactionsValue = (Tuple.first transactionsItem) ++ (Tuple.second transactionsItem)
    _ = Debug.log "transactionsValue" transactionsValue
  in
    List.sum transactionsValue

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of 
        NoOp ->
            ( model, Cmd.none )
        LoadingQuotations ->
            ({ model | loading = True }, Cmd.none)
        LoadedQuotations ->
            ({ model | loading = False }, Cmd.none)
        UpdateName name ->
            (updateCustomer (\customer -> { customer | name = name }) model, Cmd.none)
        UpdateEmail email ->
           (updateCustomer (\customer -> { customer | email = email }) model, Cmd.none)
        SaveCustomer ->
            (updateCustomer (\customer -> { customer | wallet = 100000.00, stored = True }) model, Cmd.none)
        QuotationsMsg modelMsg ->
          case modelMsg of
            Quotations.LoadQuotation -> 
              (model, Cmd.none)
            Quotations.GotBrita brita ->
              case brita of 
                  Ok data ->
                    let
                        oldQuotations = model.quotations
                        newQuotations = { oldQuotations | brita = data }
                    in
                      ({ model | quotations = newQuotations }, Cmd.none)
                  Err error ->
                    let
                        _ = Debug.log "Error" error
                    in
                      (model, Cmd.none)
            Quotations.GotBitcoin bitcoin ->
              case bitcoin of 
                  Ok data ->
                    let
                        oldQuotations = model.quotations
                        newQuotations = { oldQuotations | bitcoin = data }
                    in
                      ({ model | quotations = newQuotations }, Cmd.none)
                  Err error ->
                    let
                        _ = Debug.log "Error" error
                    in
                      (model, Cmd.none)
        CoinsMsg modelMsg ->
          case modelMsg of 
            Coins.SelectCoin coin  ->
              let
                oldCoins = model.coinActions
                newCoins = { oldCoins | selected = coin, value = 0  }
              in
                ({model | coinActions = newCoins }, Cmd.none)
            Coins.SetOperation operation  ->
              let
                oldCoins = model.coinActions
                newCoins = { oldCoins | operation = operation }
              in
                ({model | coinActions = newCoins }, Cmd.none)
            Coins.UpdateValue quantity -> 
              let
                oldCoins = model.coinActions
                newCoins = { oldCoins | value = Maybe.withDefault 0 (String.toFloat quantity) }
              in
                ({model | coinActions = newCoins }, Cmd.none)
            Coins.BuyCoin -> 
              executeOperationCoin model "Buy"
            Coins.SellCoin -> 
              executeOperationCoin model "Sell"

        TransactionsMsg modelMsg ->
          case modelMsg of
            Transactions.CreateTransaction operation ->
              let
                moneyCustomer = model.customer.wallet
                value = model.coinActions.value
                calcCoinMoney = Coins.calculateTotalCoin model.coinActions model.quotations
                transaction = case List.head model.transactions of
                    Just result ->
                      let
                        money = 
                          if operation == "Buy" then
                            result.money + calcCoinMoney
                          else 
                            result.money - calcCoinMoney
                      in
                        {
                          coin = model.coinActions.selected, 
                          quantity = value, 
                          money = money, 
                          operation = operation
                        }
                    Nothing ->
                      let
                        money = 
                          if operation == "Buy" then
                            moneyCustomer + calcCoinMoney
                          else 
                            moneyCustomer - calcCoinMoney
                      in                        
                        {
                          coin = model.coinActions.selected, 
                          quantity = value, 
                          money = money, 
                          operation = operation
                        }
                oldTransactions = [ transaction ] ++ model.transactions
                _ = Debug.log "partition" List.partition (\x -> x.coin == "Brita") oldTransactions
              in
                ({model | transactions = oldTransactions}, Cmd.none)       

view: Model -> Html Msg
view model =
  div []
    [
        label [] [text "nome do cliente"]
        ,   input [ placeholder "abc", onInput UpdateName] []
        ,   label [] [text "email do cliente"]
        ,   input [ placeholder "abc", onInput UpdateEmail] []
        ,   button [onClick SaveCustomer] [text "Criar Conta"]
        ,   button [onClick (QuotationsMsg Quotations.LoadQuotation)] [text "Criar"]
        ,   if model.customer.stored == True then 
              Html.map CoinsMsg (Coins.view model.coinActions model.quotations)
            else 
              text ""
        , text (Debug.toString model)
        , text (String.fromFloat (getTransactions model.transactions "Brita"))
        , text "----"
        , text (String.fromFloat (getTransactions model.transactions "Bitcoin"))
    ]

subscriptions : Model -> Sub Msg
subscriptions model = Sub.none

main =
  Browser.element 
  { 
    init = initialState, 
    subscriptions = subscriptions,
    update = update, 
    view = view 
  }
