module Coins exposing (..)

import Html exposing (..)
import Html.Attributes exposing(..)
import Html.Events exposing (..)
import GetQuotations as Quotations 

type alias Model =
    { 
      value: Float,
      operation: String,
      selected: String
    }


type Msg
    = SelectCoin String
    | SetOperation String
    | UpdateValue String
    | BuyCoin
    | SellCoin 

getTaxBrita : String -> Quotations.Brita -> Float -> Float
getTaxBrita operation quotation quantity = 
  let
    tax = case List.head quotation.value of
        Just value -> 
          value
        Nothing ->
          Quotations.BritaFields 0.0 0.0 "0.0"
  in
    if operation == "Sell" then
     tax.cotacaoVenda * quantity
    else if operation == "Buy" then
     tax.cotacaoCompra * quantity
    else
      0.0

getTaxBitcoin : String -> Quotations.Bitcoin -> Float -> Float
getTaxBitcoin operation quotation quantity = 
  let
    tax = quotation.data
  in
    if operation == "Sell" then
     tax.sell * quantity
    else if operation == "Buy" then
     tax.buy * quantity
    else
      0.0

calculateTotalCoin : Model -> Quotations.Model -> Float
calculateTotalCoin model quotations = 
  let
    value = model.value
    tax = case model.selected of
        "Brita" ->
          getTaxBrita model.operation quotations.brita value
        "Bitcoin" ->
          getTaxBitcoin model.operation quotations.bitcoin value
        _ ->
          0.0
    _ = Debug.log "quotation.data" tax
  in
    tax
  

-- disabledIFNotEnoughCoins : Model -> Bool
-- disabledIFNotEnoughCoins model = 
--   if 
--     model.selected == "Brita" 
--     && (model.quantityBrita > model.quantityBritaBought
--     || model.quantityBritaBought == 0) then
--     True
--   else if model.selected == "Bitcoin" 
--     && (model.quantityBitcoin > model.quantityBitcoinBought
--     || model.quantityBitcoinBought == 0) then
--     True
--   else 
--     False

numberInput : (String -> Msg) -> Float -> Html Msg   
numberInput msg inputValue =
  input [ type_ "number", placeholder "Total", onInput msg, value (String.fromFloat inputValue)] []

view : Model -> Quotations.Model -> Html Msg
view model quotations =
    div []
      [ 
        label [for "brita_coin"] [text "Brita"]
        , input [type_ "radio", id "brita_coin", name "coin", value "Brita", onClick (SelectCoin "Brita") ] [] 
        , label [for "bitcoin_coin"] [text "Bitcoin"]
        , input [type_ "radio", id "bitcoin_coin", value "Bitcoin", name "coin", onClick (SelectCoin "Bitcoin")] [] 
        , if model.selected /= "" then 
          div [] [
            label [for "buy_coin"] [text "Comprar"]
            , input [type_ "radio", id "buy_coin", name "operation_coin", onClick (SetOperation "Buy") ] [] 
            , label [for "sell_coin"] [text "Vender"]
            ,input [type_ "radio", id "sell_coin", name "operation_coin", onClick (SetOperation "Sell")] [] 
        ]
        else
          text ""
        , text (String.fromFloat(calculateTotalCoin model quotations))
        , if model.operation /= "" then
            numberInput UpdateValue model.value
          else 
            text ""
        , if model.operation == "Buy" then
            button [ onClick BuyCoin ] [ text "Comprar" ]
          else 
            button [ onClick SellCoin ] [ text "Vender" ]
      ]
